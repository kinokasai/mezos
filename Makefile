all: 3

3:
	TEZOS_PROTO=3 dune build @install @test/runtest

2:
	TEZOS_PROTO=2 dune build @install @test/runtest

1:
	TEZOS_PROTO=1 dune build @install @test/runtest

alpha:
	TEZOS_PROTO=0 dune build @install @test/runtest

build-deps:
	opam pin add sqlexpr --dev-repo -y
	opam pin add ppx_sqlexpr --dev-repo -y
	opam install gen logs fmt rresult cmdliner -y

clean:
	dune clean
