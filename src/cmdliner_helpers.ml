(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

let setup_log level logs =
  Logs.set_level level ;
  Logs.set_reporter (Logs_lwt_ovh.udp_reporter ?logs ())

open Cmdliner
open Rresult

module Convs = struct
  let uri =
    let parser v = R.ok (Uri.of_string v) in
    Arg.conv ~docv:"URL" (parser, Uri.pp_hum)

  let bip32_node =
    let bip32_node_of_string v =
      match Int32.of_string v with
      | exception _ -> begin
          match Int32.of_string String.(sub v 0 (length v - 1)) with
          | exception _ -> Error "Invalid BIP32 node"
          | v -> Ok (Int32.logor 0x8000_0000l v)
        end
      | v -> Ok v in
    let pp_bip32_node ppf node =
      if node < 0l then
        Format.fprintf ppf "%ld'" Int32.(add min_int node)
      else
        Format.fprintf ppf "%ld" node in
    let parser v =
      R.reword_error (fun s -> `Msg s) (bip32_node_of_string v) in
    let printer = pp_bip32_node in
    Arg.conv ~docv:"BIP32 NODE" (parser, printer)

  let bip32_path =
    let open Bip32_ed25519.Human_readable in
    let parser v =
      match path_of_string v with
      | None -> Error (`Msg "Invalid BIP32 path")
      | Some path -> Ok path in
    Arg.conv ~docv:"BIP32 PATH" (parser, pp_path)
end

module Terms = struct
  let ovh_log =
    let doc = "URL and token for OVH Logs service." in
    Arg.(value
         & opt (some (t2 Convs.uri string)) None
         & info ["ovh-logs"] ~doc ~docv: "URL,TOKEN")

  let setup_log =
    Term.(const setup_log $ Logs_cli.level () $ ovh_log)

  let tezos_client_dir =
    Arg.(value
         & opt string (Filename.concat (Sys.getenv "HOME") ".tezos-client")
         & info ["tezos-client-dir"]
           ~doc:"Where Tezos client config resides"
           ~docv:"PATH")

  let datadir =
    Arg.(value
         & opt string (Filename.concat (Sys.getenv "HOME") ".mezos")
         & info ["d"; "datadir"]
           ~doc:"Where Mezos data will be stored"
           ~docv:"PATH")

  let force ~doc =
    Arg.(value & flag & info ["f"; "force"] ~doc)

  let host ?(argnames=["h"; "host"]) ~default ~doc () =
    Arg.(value & opt string default & info argnames ~doc ~docv:"HOST")

  let port ?(argnames=["p"; "port"]) ~default ~doc () =
    Arg.(value & opt int default & info argnames ~doc ~docv:"PORT")

  let cfg_file =
    let base_dir = Filename.concat (Sys.getenv "HOME") ".tezos-client" in
    let default = Filename.concat base_dir "config" in
    let doc = "`tezos-client` config file" in
    Arg.(value & opt string default & info ["c"; "cfg"] ~doc ~docv:"FILENAME")

  let uri ~doc ~args =
    Arg.(value & opt (some Convs.uri) None & info args ~doc ~docv: "URL")

  let tls =
    Arg.(value & flag &
         info ["s"; "tls"] ~doc:"use TLS to connect to Tezos node")

  let alias_pos position =
    let doc = "Wallet name to use." in
    Arg.(required & pos position (some string) None & info [] ~docv:"WALLET" ~doc)

  let alias_opt =
    let doc = "Wallet name to use." in
    Arg.(value & opt (some string) None & info ["a"; "alias"] ~docv:"WALLET" ~doc)
end

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
