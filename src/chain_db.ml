(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Mezos_utils
module AC = Alpha_context
module Sqlexpr = Sqlexpr_sqlite_lwt

let db_name = "chain.db"
let src = Logs.Src.create "mezos.chain_db"

let _ = [
  [%sqlinit "create table chain (hash text primary key) \
             without rowid"] ;

  [%sqlinit "create table block (hash text primary key, \
             level int not null, \
             proto int not null, \
             predecessor text not null, \
             timestamp int not null, \
             validation_passes int not null, \
             operation_hash text not null, \
             fitness text not null, \
             context text not null) without rowid"] ;

  [%sqlinit "create index block_level on block (level)"] ;


  [%sqlinit "create table operation (hash text primary key, \
             chain text not null, \
             blk text not null, \
             foreign key (chain)  references chain (hash), \
             foreign key (blk)    references block (hash)) without rowid"] ;

  [%sqlinit "create index operation_chain on operation (chain)"] ;
  [%sqlinit "create index operation_blk   on operation (blk)"] ;


  [%sqlinit "create table operation_alpha (hash text not null, \
             id int not null, \
             cat int not null, \
             primary key (hash, id), \
             foreign key (hash) references operation (hash))"] ;

  [%sqlinit "create index operation_alpha_cat on operation_alpha (cat)"] ;


  [%sqlinit "create table implicit (pkh text primary key, \
             activated text, \
             revealed text, \
             pk text, \
             foreign key (activated) references block (hash), \
             foreign key (revealed)  references block (hash)) without rowid"] ;

  [%sqlinit "create index implicit_activated on implicit (activated)"] ;
  [%sqlinit "create index implicit_revealed on implicit (revealed)"] ;


  [%sqlinit "create table endorsement (blk text, \
             op text, \
             id int, \
             level int, \
             pkh text, \
             slot int, \
             primary key (blk, op, id, level, pkh, slot), \
             foreign key (blk) references block (hash), \
             foreign key (op)  references operation (hash), \
             foreign key (pkh) references implicit (pkh)) without rowid"] ;


  [%sqlinit "create table block_alpha (hash text primary key, \
             baker text not null, \
             level_position int not null, \
             cycle int not null, \
             cycle_position int not null, \
             voting_period int not null, \
             voting_period_position int not null, \
             voting_period_kind int not null, \
             consumed_gas int not null, \
             foreign key (hash)  references block    (hash), \
             foreign key (baker) references implicit (pkh)) without rowid"] ;

  [%sqlinit "create index block_alpha_baker          on block_alpha (baker)"] ;
  [%sqlinit "create index block_alpha_level_position on block_alpha (level_position)"] ;
  [%sqlinit "create index block_alpha_cycle          on block_alpha (cycle)"] ;
  [%sqlinit "create index block_alpha_cycle_position on block_alpha (cycle_position)"] ;


  [%sqlinit "create table deactivated (pkh text not null, \
             blk text not null, \
             primary key (pkh, blk), \
             foreign key (pkh) references implicit (pkh), \
             foreign key (blk) references block (hash)) without rowid"] ;


  [%sqlinit "create table contract (k text primary key, \
             blk text not null, \
             mgr text, \
             delegate text, \
             spendable bool, \
             delegatable bool, \
             credit int, \
             preorig text, \
             script text, \
             foreign key (blk) references block (hash), \
             foreign key (mgr) references implicit (pkh), \
             foreign key (delegate) references implicit (pkh), \
             foreign key (preorig) references contract (k)) without rowid"] ;

  [%sqlinit "create index contract_blk on contract (blk)"] ;
  [%sqlinit "create index contract_mgr on contract (mgr)"] ;
  [%sqlinit "create index contract_delegate on contract (delegate)"] ;
  [%sqlinit "create index contract_preorig on contract (preorig)"] ;


  [%sqlinit "create table tx (op text not null, \
             op_id int not null, \
             source text not null, \
             destination text not null, \
             fee int not null, \
             amount int not null, \
             primary key (op, op_id), \
             foreign key (op, op_id) references operation_alpha (hash, id), \
             foreign key (source) references contract (k), \
             foreign key (destination) references contract (k))"] ;

  [%sqlinit "create index tx_source       on tx (source)"] ;
  [%sqlinit "create index tx_destination  on tx (destination)"] ;


  [%sqlinit "create table origination (op text not null, \
             op_id int not null, \
             source text not null, \
             k text not null, \
             primary key (op, op_id), \
             foreign key (op, op_id) references operation_alpha (hash, id), \
             foreign key (source) references contract (k), \
             foreign key (k) references contract (k))"] ;

  [%sqlinit "create index origination_source on origination (source)"] ;
  [%sqlinit "create index origination_k on origination (k)"] ;


  [%sqlinit "create table delegation (op text not null, \
             op_id int not null, \
             source text not null, \
             pkh text, \
             primary key (op, op_id), \
             foreign key (op, op_id) references operation_alpha (hash, id), \
             foreign key (source) references contract (k), \
             foreign key (pkh) references implicit (pkh))"] ;

  [%sqlinit "create index delegation_source on delegation (source)"] ;
  [%sqlinit "create index delegation_pkh on delegation (pkh)"] ;


  [%sqlinit "create table balance (blk text not null, \
             op text, \
             op_id int, \
             cat int not null, \
             k text not null, \
             cycle int, \
             diff int not null, \
             foreign key (blk)       references block (hash), \
             foreign key (op, op_id) references operation_alpha (hash, id), \
             foreign key (k)         references contract (k))"] ;

  [%sqlinit "create index balance_blk   on balance (blk)"] ;
  [%sqlinit "create index balance_op    on balance (op, op_id)"] ;
  [%sqlinit "create index balance_cat   on balance (cat)"] ;
  [%sqlinit "create index balance_k     on balance (k)"] ;
  [%sqlinit "create index balance_cycle on balance (cycle)"] ;

  [%sqlinit "create table snapshot (cycle int, \
             level int, \
             primary key (cycle, level)) without rowid"] ;

  [%sqlinit "create table delegate (cycle int not null, \
             level int not null, \
             pkh text not null, \
             balance int not null, \
             frozen_balance int not null, \
             staking_balance int not null, \
             delegated_balance int not null, \
             deactivated bool not null, \
             grace int not null, \
             primary key (cycle, pkh), \
             foreign key (cycle, level) references snapshot (cycle, level), \
             foreign key (pkh)   references implicit (pkh)) without rowid"] ;

  [%sqlinit "create table delegated_contract (delegate text, \
             delegator text, \
             cycle int, \
             level int, \
             primary key (delegate, delegator, cycle, level), \
             foreign key (delegate) references implicit (pkh), \
             foreign key (delegator) references contract (k), \
             foreign key (cycle, level) references snapshot (cycle, level)) \
             without rowid"] ;

  [%sqlinit "create index delegated_contract_cycle on delegated_contract (cycle)"] ;
  [%sqlinit "create index delegated_contract_level on delegated_contract (level)"] ;

  [%sqlinit "create table stake (delegate text not null, \
             level int not null, \
             k        text not null, \
             kind int not null, \
             diff     int not null, \
             primary key (delegate, level, k, kind, diff), \
             foreign key (delegate) references implicit (pkh), \
             foreign key (k)        references contract (k))"] ;

  (* VIEWS *)

  [%sqlinit "create view level as \
             select level, hash from block \
             order by level asc"] ;

  [%sqlinit "create view tx_full as \
             select op, op_id, \
             b.hash as blk, \
             b.level as level, \
             b.timestamp as timestamp, \
             source, k1.mgr as source_mgr, \
             destination, k2.mgr as destination_mgr, fee, amount \
             from tx \
             join operation on tx.op = operation.hash \
             join block b on operation.blk = b.hash \
             join contract k1 on tx.source = k1.k \
             join contract k2 on tx.destination = k2.k"] ;

  [%sqlinit "create view balance_full as \
             select block.level, block_alpha.cycle, \
             cycle_position, op, op_id, k, cat, diff \
             from block \
             natural join block_alpha \
             join balance on block.hash = balance.blk"]
]

let select_balance_full_stmt =
  [%sqlc "select @l{level}, @l{cycle}, \
          @l{cycle_position}, @s?{op}, @d?{op_id}, @s{k}, @d{cat}, @L{diff} \
          from balance_full where k = %s and level >= %l"]

let update_block_stmt =
  [%sqlc "insert into block values (%s, %l, %d, %s, %L, %d, %s, %s, %s) \
          on conflict do nothing"]

let update_stake_stmt =
  [%sqlc "insert into stake values (%s, %l, %s, %d, %L) \
          on conflict do nothing"]

let select_stake_full_stmt =
  [%sqlc "select @s{k}, @l{level}, @d{kind}, @L{diff} \
          from stake where delegate = %s"]

let select_stake_stmt =
  [%sqlc "select @l{level}, @d{kind}, @L{diff} \
          from stake where delegate = %s and k = %s"]

let select_tx_full_stmt =
  [%sqlc "select @s{op}, \
          @d{op_id}, @s{blk}, @d{level}, @L{timestamp}, \
          @s{source}, @s?{source_mgr}, \
          @s{destination}, @s?{destination_mgr}, \
          @L{fee}, @L{amount} from tx_full \
          where source = %s or destination = %s" ]

let update_tx_stmt =
  [%sqlc "insert or replace into tx values (%s, %d, %s, %s, %L, %L)"]

let select_contract_stmt =
  [%sqlc "select @s{k}, @s{blk}, @s{mgr}, @s?{delegate}, \
          @b{spendable}, @b{delegatable}, @L{credit}, @s?{preorig} from contract"]

let select_contract_infos_stmt =
  [%sqlc "select @s{blk}, @s{mgr}, @s?{delegate}, \
          @b{spendable}, @b{delegatable}, @L{credit}, \
          @s?{preorig}, @s?{script} \
          from contract where k = %s"]

let select_contract_with_mgr_stmt =
  [%sqlc "select @s{k} from contract where mgr = %s"]

let update_contract_only_stmt =
  [%sqlc "insert into contract (k, blk) values (%s, %s) \
          on conflict do nothing"]

let update_contract_stmt =
  [%sqlc "insert or replace into contract values (%s, %s, %s, %s?, %b, %b, %L, %s?, %s?)"]

let discovered_implicit_stmt =
  [%sqlc "insert into implicit (pkh) values (%s) \
          on conflict do nothing"]

let discovered_contract_stmt =
  [%sqlc "insert into contract (k, blk, mgr) values (%s, %s, %s) \
          on conflict do nothing"]

let activated_implicit_stmt =
  [%sqlc "insert into implicit (pkh, activated) values (%s, %s) \
          on conflict (pkh) \
          do update set activated = %s"]

let revealed_implicit_stmt =
  [%sqlc "insert into implicit (pkh, revealed, pk) values (%s, %s, %s) \
          on conflict (pkh) \
          do update set (revealed, pk) = (%s, %s)"]

let select_level_stmt =
  [%sqlc "select @l{level}, @s{hash} from level"]

let select_max_level_stmt =
  [%sqlc "select max(@l?{level}) from block"]

let update_contract_delegate_stmt =
  [%sqlc "update contract set delegate = %s? where k = %s"]

let update_delegate_stmt =
  [%sqlc "insert or replace into delegate values (%l, %l, %s, %L, %L, %L, %L, %b, %l)"]

let select_delegate_stmt =
  [%sqlc "select @l{cycle}, @l{level}, @L{balance}, \
          @L{frozen_balance}, @L{staking_balance}, \
          @L{delegated_balance}, @b{deactivated}, @l{grace} \
          from delegate where pkh = %s"]

let update_chain_stmt =
  [%sqlc "insert into chain values (%s) on conflict do nothing"]

let update_operation_stmt =
  [%sqlc "insert into operation values (%s, %s, %s) on conflict do nothing"]

let update_operation_alpha_stmt =
  [%sqlc "insert into operation_alpha values (%s, %d, %d) on conflict do nothing"]

let update_deactivated_stmt =
  [%sqlc "insert into deactivated values (%s, %s) on conflict do nothing"]

let update_block_alpha_stmt =
  [%sqlc "insert into block_alpha values (%s, %s, %l, %l, %l, %l, %l, %d, %L) \
          on conflict do nothing"]

let update_origination_stmt =
  [%sqlc "insert into origination values (%s, %d, %s, %s) \
          on conflict do nothing"]

let update_delegation_stmt =
  [%sqlc "insert into delegation values (%s, %d, %s, %s?) \
          on conflict do nothing"]

let update_balance_stmt =
  [%sqlc "insert into balance values (%s, %s?, %d?, %d, %s?, %l?, %L) \
          on conflict do nothing"]

let update_snapshot_stmt =
  [%sqlc "insert into snapshot values (%d, %l)"]

let select_snapshot_stmt =
  [%sqlc "select @l{cycle}, @l{level} from snapshot"]

let update_delegated_contract_stmt =
  [%sqlc "insert into delegated_contract values (%s, %s, %l, %l) \
          on conflict do nothing"]

let count_operation_alpha_stmt =
  [%sqlc "select @d{count()} from operation_alpha where hash = %s"]

let nb_alpha_operations db oph =
  Sqlexpr.select_one db count_operation_alpha_stmt
    (Operation_hash.to_b58check oph)

let store_snapshot_levels db =
  Lwt_list.iter_s begin fun (c, l) ->
    Sqlexpr.execute db update_snapshot_stmt c l
  end

let snapshot_levels db =
  let open Alpha_context in
  Sqlexpr.fold db begin fun cm (cycle, level) ->
    Lwt.return (Cycle.Map.add (Obj.magic cycle) level cm)
  end Cycle.Map.empty select_snapshot_stmt

let update_stake ~delegate ~level ~k ~kind ~amount db =
  let open Alpha_context in
  Sqlexpr.execute db update_stake_stmt
    (Signature.Public_key_hash.to_b58check delegate)
    level
    (Contract.to_b58check k)
    (match kind with `Contract -> 0 | `Rewards -> 1)
    amount

let fold_stake_full
    ?(max_level=Int32.max_int) db ~delegate ~f ~init =
  let open Alpha_context in
  let ret = ref init in
  Lwt.catch begin fun () ->
    Sqlexpr.fold db begin fun a (k, level, kind, amount) ->
      if level > max_level then begin
        ret := a ;
        raise Exit
      end ;
      match Contract.of_b58check k with
      | Error _ -> assert false
      | Ok k ->
        let kind = match kind with
          | 0 -> `Contract
          | _ -> `Rewards in
        f a ~k ~level ~kind ~amount
    end init select_stake_full_stmt
      (Signature.Public_key_hash.to_b58check delegate)
  end begin function
    | Exit -> Lwt.return !ret
    | e -> Lwt.fail e
  end

let fold_stake db ~delegate ~k ~f ~init =
  let open Alpha_context in
  Sqlexpr.fold db begin fun a (level, kind, amount) ->
    let kind = match kind with
      | 0 -> `Contract
      | _ -> `Rewards in
    f a ~level ~kind ~amount
  end init select_stake_stmt
    (Signature.Public_key_hash.to_b58check delegate)
    (Contract.to_b58check k)

(* let update_endorsement_stmt =
 *   [%sqlc "insert into endorsement values (%s, %s, %d, %l, %s, %d) \
 *           on conflict do nothing"] *)

let auto_init_db, check_db, auto_check_db = [%sqlcheck "sqlite"]

open Alpha_context

type tx = {
  op_hash : Operation_hash.t ;
  id : int ;
  blk_hash : Block_hash.t ;
  level : int ;
  timestamp : Time.t ;
  src : Contract.t ;
  src_mgr : Signature.public_key_hash option ;
  dst : Contract.t ;
  dst_mgr : Signature.public_key_hash option ;
  amount : Tez.t ;
  fee : Tez.t ;
}

(* let create_tx
 *     ~op_hash ~id ~blk_hash ~level
 *     ~timestamp ~src ?src_mgr ~dst ?dst_mgr ~amount ~fee () =
 *   { op_hash ; id ; blk_hash ; level ; timestamp ;
 *     src ; src_mgr ; dst ; dst_mgr ; amount ; fee } *)

let tx_encoding =
  let open Data_encoding in
  conv
    (fun { op_hash; id; blk_hash; level; timestamp; src; src_mgr;
           dst; dst_mgr ; amount; fee } ->
      op_hash, (id, blk_hash, level, timestamp, src, src_mgr,
                dst, dst_mgr, amount, fee))
    (fun (op_hash, (id, blk_hash, level, timestamp, src, src_mgr,
                    dst, dst_mgr, amount, fee)) ->
      { op_hash; id; blk_hash; level; timestamp; src; src_mgr;
        dst; dst_mgr; amount; fee })
    (merge_objs
       (obj1
          (req "op_hash" Operation_hash.encoding))
       (obj10
          (req "id" uint16)
          (req "blk_hash" Block_hash.encoding)
          (req "level" int31)
          (req "timestamp" Time.encoding)
          (req "src" Contract.encoding)
          (opt "src_mgr" Signature.Public_key_hash.encoding)
          (req "dst" Contract.encoding)
          (opt "dst_mgr" Signature.Public_key_hash.encoding)
          (req "amount" Tez.encoding)
          (req "fee" Tez.encoding)))

let create_tx_from_db
    a (op_hash, id, blk_hash, level, timestamp,
       source, source_mgr, dst, dst_mgr, amount, fee) =
  let op_hash = Operation_hash.of_b58check_exn op_hash in
  let blk_hash = Block_hash.of_b58check_exn blk_hash in
  let timestamp = Time.of_seconds timestamp in
  match
    Contract.of_b58check source,
    (Option.map ~f:Signature.Public_key_hash.of_b58check_exn source_mgr),
    Contract.of_b58check dst,
    (Option.map ~f:Signature.Public_key_hash.of_b58check_exn dst_mgr),
    Tez.of_mutez amount,
    Tez.of_mutez fee
  with
  | Ok src, src_mgr, Ok dst, dst_mgr, Some amount, Some fee ->
    Lwt.return @@ Operation_hash.Map.update op_hash begin function
      | None -> Some (IntMap.singleton id
                        { op_hash ; id ; blk_hash ; level ; timestamp ;
                          src ; src_mgr ; dst ; dst_mgr ; amount ; fee })
      | Some txmap ->
        Some (IntMap.add id { op_hash ; id ; blk_hash ; level ; timestamp ;
                              src ; src_mgr ; dst ; dst_mgr ; amount ; fee } txmap)
    end a
  | _ ->
    Logs_lwt.debug ~src begin fun m ->
      m "create_tx_from_db: %s -> %s" source dst
    end >>= fun () ->
    Lwt.return a

let find_txs_involving_k db k =
  let k_str = Contract.to_b58check k in
  Sqlexpr.fold db
    create_tx_from_db
    Operation_hash.Map.empty
    select_tx_full_stmt k_str k_str

let update_contract db k k_str blk_str =
  begin match Contract.is_implicit k with
    | None -> (* originated *)
      Sqlexpr.execute db update_contract_only_stmt k_str blk_str
    | Some _ -> (* implicit account *)
      Sqlexpr.execute db discovered_implicit_stmt k_str >>= fun () ->
      Sqlexpr.execute db discovered_contract_stmt k_str blk_str k_str
  end >>= fun () ->
  return_unit

module Balance_update = struct
  type t = (Delegate.balance * Delegate.balance_update) list

  let int_of_balance = function
    | Delegate.Contract _ -> 0
    | Rewards _ -> 1
    | Fees _ -> 2
    | Deposits _ -> 3

  let avg_tezs tezs =
    let open Alpha_context.Tez in
    let open Alpha_environment in
    fold_left_s begin fun (s, n) t ->
      Lwt.return (wrap_error (s +? t)) >>|? fun sum ->
      sum, Int64.succ n
    end (zero, 0L) tezs >>=? fun (s, n) ->
    Lwt.return (wrap_error (s /? n))

  let sum_tezs tezs =
    let open Alpha_context.Tez in
    let open Alpha_environment in
    fold_left_s begin fun s t ->
      Lwt.return (wrap_error (s +? t))
    end zero tezs

  (* let sum_balance_updates bas =
   *   let open Alpha_context.Tez in
   *   let open Alpha_environment in
   *   fold_left_s begin fun s -> function
   *     | Delegate.Credited t -> Lwt.return (wrap_error (s +? t))
   *     | Debited t -> Lwt.return (wrap_error (s -? t))
   *   end zero bas *)

  (* let all_contracts (t : t) =
   *   List.fold_left begin fun a -> function
   *     | Delegate.Contract k, ba -> (k, ba) :: a
   *     | _ -> a
   *   end [] t *)

  (* let contract (t : t) k =
   *   List.fold_left begin fun a -> function
   *     | Delegate.Contract k', ba when k = k' -> ba :: a
   *     | _ -> a
   *   end [] t
   * 
   * let all_deposits (t : t) =
   *   List.fold_left begin fun a -> function
   *     | Delegate.Deposits (pkh, c), ba -> ((pkh, c), ba) :: a
   *     | _ -> a
   *   end [] t
   * 
   * let deposit ?cycle (t : t) pkh =
   *   List.fold_left begin fun a -> function
   *     | Delegate.Deposits (pkh', c), ba when pkh = pkh' ->
   *       begin match cycle with
   *         | None -> ba :: a
   *         | Some c' when c = c' -> ba :: a
   *         | _ -> a
   *       end
   *     | _ -> a
   *   end [] t *)

  let update_tables ?op_info db ~blk_hash_str (t: t) =
    let op_hash_str, op_id =
      match op_info with
      | None -> None, None
      | Some (op_hash_str, op_id) -> Some op_hash_str, Some op_id in
    let get_diff diff =
      match diff with
      | Delegate.Credited amount -> Tez.to_mutez amount
      | Debited amount -> Int64.neg (Tez.to_mutez amount) in
    Lwt_list.iter_s begin fun (b, diff) ->
      match b with
      | Delegate.Contract k ->
        Sqlexpr.execute db
          update_balance_stmt
          blk_hash_str op_hash_str op_id
          (int_of_balance b)
          (Some (Contract.to_b58check k))
          None
          (get_diff diff)
      | Rewards (pkh, cycle)
      | Fees (pkh, cycle)
      | Deposits (pkh, cycle) ->
        Sqlexpr.execute db
          update_balance_stmt
          blk_hash_str op_hash_str op_id
          (int_of_balance b)
          (Some (Signature.Public_key_hash.to_b58check pkh))
          (Some (Cycle.to_int32 cycle))
          (get_diff diff)
    end t
end

let process_mgr_operation :
  type a b.
  Sqlexpr.db -> string -> string -> int -> Contract.t -> Tez.t ->
  a manager_operation ->
  b Apply_results.successful_manager_operation_result -> unit tzresult Lwt.t =
  fun db blk_hash_str op_hash_str i source fee operation operation_result ->
  let src_str = Contract.to_b58check source in
  let process_tx amount destination balance_updates originated_contracts =
    (* Add discovered contracts *)
    Lwt_list.iter_s begin fun k ->
      Sqlexpr.execute db
        update_contract_only_stmt (Contract.to_b58check k) blk_hash_str
    end originated_contracts >>= fun () ->
    let dst_str = Contract.to_b58check destination in
    (* Add src and dst *)
    update_contract db source src_str blk_hash_str >>=? fun () ->
    update_contract db destination dst_str blk_hash_str >>=? fun () ->
    (* Update balances *)
    Balance_update.update_tables
      ~op_info:(op_hash_str, i) db
      ~blk_hash_str balance_updates >>= fun () ->
    (* Update tx *)
    Sqlexpr.execute db update_tx_stmt
      op_hash_str i
      src_str dst_str
      (Tez.to_mutez amount)
      (Tez.to_mutez fee)
    >>= fun () ->
    Logs_lwt.debug ~src begin fun m ->
      m "Stored TX %s (%d)" op_hash_str i
    end >>= fun () ->
    return_unit in
  let process_origination
      manager delegate spendable delegatable
      credit preorigination script balance_updates originated_contracts =
    let script_json_str =
      Option.map script ~f:begin fun script ->
        Data_encoding.Json.(construct Script.encoding script |> to_string)
      end in
    assert (List.length originated_contracts = 1) ;
    let k_str = Contract.to_b58check (List.hd originated_contracts) in
    let manager_str =
      Signature.Public_key_hash.to_b58check manager in
    let maybe_delegate_str =
      Option.map ~f:Signature.Public_key_hash.to_b58check delegate in
    (* Add discovered implicits *)
    Sqlexpr.execute db discovered_implicit_stmt manager_str >>= fun () ->
    begin match maybe_delegate_str with
      | None -> Lwt.return_unit
      | Some delegate_str ->
        Sqlexpr.execute db discovered_implicit_stmt delegate_str
    end >>= fun () ->
    (* Insert full contract row *)
    Sqlexpr.execute db update_contract_stmt
      k_str
      blk_hash_str
      manager_str
      maybe_delegate_str
      spendable delegatable
      (Tez.to_mutez credit)
      (Option.map ~f:Contract.to_b58check preorigination)
      script_json_str
    >>= fun () ->
    (* Insert in origination table *)
    Sqlexpr.execute db update_origination_stmt
      op_hash_str i src_str k_str >>= fun () ->
    (* Update balances *)
    Balance_update.update_tables
      ~op_info:(op_hash_str, i) db
      ~blk_hash_str balance_updates >>= fun () ->
    Logs_lwt.debug ~src begin fun m ->
      m "Stored origination of %s" src_str
    end >>= fun () ->
    return_unit in
  match operation, operation_result with
  | Transaction { amount ; destination },
    Apply_results.Transaction_result
      { balance_updates ; originated_contracts } ->
    process_tx amount destination balance_updates originated_contracts
  | Reveal pk, _ ->
    let pk_str = Signature.Public_key.to_b58check pk in
    Sqlexpr.execute db revealed_implicit_stmt
      src_str blk_hash_str pk_str blk_hash_str pk_str
    >>= fun () ->
    Logs_lwt.debug ~src begin fun m ->
      m "Stored revelation of %s" pk_str
    end >>= fun () ->
    return_unit
  | Origination { manager; delegate; spendable;
                  delegatable; credit; preorigination ; script },
    Apply_results.Origination_result { balance_updates ;
                                       originated_contracts } ->
    process_origination
      manager delegate spendable delegatable
      credit preorigination script balance_updates originated_contracts
  | Delegation maybe_pkh, _ ->
    let maybe_pkh_str =
      Option.map ~f:Signature.Public_key_hash.to_b58check maybe_pkh in
    Sqlexpr.execute db update_delegation_stmt
      op_hash_str i src_str maybe_pkh_str >>= fun () ->
    Sqlexpr.execute db update_contract_delegate_stmt
      maybe_pkh_str src_str >>= fun () ->
    Logs_lwt.debug ~src begin fun m ->
      m "Stored delegation %s -> %s"
        src_str (Option.unopt maybe_pkh_str ~default:"")
    end >>= fun () ->
    return_unit
  | _, _ -> assert false

let store_op_id :
  type a b. Sqlexpr.db -> Operation_hash.t -> int -> Block_hash.t ->
  a contents -> b Apply_results.contents_result -> unit tzresult Lwt.t =
  fun db op_hash i blk_hash contents meta ->
  let op_hash_str = Operation_hash.to_b58check op_hash in
  let blk_hash_str = Block_hash.to_b58check blk_hash in
  let int_of_contents : type a. a contents -> int = function
    | Endorsement _ -> 0
    | Seed_nonce_revelation _ -> 1
    | Double_endorsement_evidence _ -> 2
    | Double_baking_evidence _ -> 3
    | Activate_account _ -> 4
    | Proposals _ -> 5
    | Ballot _ -> 6
    | Manager_operation { operation; _ } ->
      match operation with
      | Reveal _ -> 7
      | Transaction _ -> 8
      | Origination _ -> 9
      | Delegation _ -> 10 in
  Sqlexpr.execute db update_operation_alpha_stmt
    op_hash_str i (int_of_contents contents) >>= fun () ->
  match contents, meta with
  | Seed_nonce_revelation _,
    Apply_results.Seed_nonce_revelation_result balance_updates ->
    Balance_update.update_tables db
      ~blk_hash_str ~op_info:(op_hash_str, i)
      balance_updates >>= fun () ->
    return_unit
  | Double_endorsement_evidence _,
    Apply_results.Double_endorsement_evidence_result balance_updates ->
    Balance_update.update_tables db
      ~blk_hash_str ~op_info:(op_hash_str, i)
      balance_updates >>= fun () ->
    return_unit
  | Double_baking_evidence _,
    Apply_results.Double_baking_evidence_result balance_updates ->
    Balance_update.update_tables db
      ~blk_hash_str ~op_info:(op_hash_str, i)
      balance_updates >>= fun () ->
    return_unit
  | Activate_account { id; _ },
    Apply_results.Activate_account_result balance_updates ->
    let pkh_str = Ed25519.Public_key_hash.to_b58check id in
    (* Add implicit *)
    Sqlexpr.execute db activated_implicit_stmt
      pkh_str
      blk_hash_str blk_hash_str >>= fun () ->
    (* Add contract *)
    Sqlexpr.execute db update_contract_only_stmt
      pkh_str blk_hash_str >>= fun () ->
    (* Update balances *)
    Balance_update.update_tables db
      ~blk_hash_str ~op_info:(op_hash_str, i)
      balance_updates >>= fun () ->
    return_unit
  | Endorsement { level = _ },
    Apply_results.Endorsement_result { balance_updates ;
                                       delegate = _ ;
                                       slots = _ } ->
    (* Update endorsements *)
    (* Lwt_list.iter_s begin fun slot ->
     *   Sqlexpr.execute db update_endorsement_stmt
     *     blk_hash_str op_hash_str i
     *     (Raw_level.to_int32 level)
     *     (Signature.Public_key_hash.to_b58check delegate) slot
     * end slots >>= fun () -> *)
    (* Update balances *)
    Balance_update.update_tables
      ~op_info:(op_hash_str, i)
      db ~blk_hash_str balance_updates >>= fun () ->
    return_unit
  | Manager_operation { source; fee; operation; _ },
    Apply_results.Manager_operation_result {
      balance_updates ; (* Correspond to the operation fees *)
      operation_result = Applied operation_result; (* matched and processed later *)
      internal_operation_results ; (* matched and processed later *)
    } ->
    let process_internal_op_results a =
      iter_s begin function
        | Apply_results.Internal_operation_result
            ({ source; operation; nonce = _ }, Applied operation_result) ->
          process_mgr_operation
            db blk_hash_str op_hash_str i
            source fee operation operation_result
        | _ ->
          (* Do not process effects of failed operations *)
          return_unit
      end a in
    process_internal_op_results internal_operation_results >>=? fun () ->
    process_mgr_operation
      db blk_hash_str op_hash_str i
      source fee operation operation_result >>=? fun () ->
    (* Update balances (global operation fee) *)
    Balance_update.update_tables db
      ~blk_hash_str ~op_info:(op_hash_str, i)
      balance_updates >>= fun () ->
    return_unit
  | Manager_operation _, _ ->
    (* failed/skipped/backtracked manager operation, ignore. *)
    return_unit
  | _, _ ->
    assert false

let store_op db blk_hash
    ({ Alpha_block_services.chain_id ; hash = op_hash ;
       protocol_data = Operation_data { contents } ; receipt }) =
  let rec inner :
    type a. int -> a contents_list ->
    operation_receipt -> unit tzresult Lwt.t =
    fun i contents receipt -> match contents, receipt with
      | Single _, Apply_results.No_operation_metadata ->
        assert false
      | Cons _, Apply_results.No_operation_metadata ->
        assert false
      | Single _, Apply_results.Operation_metadata { contents = Apply_results.Cons_result _ } ->
        assert false
      | Cons _, Apply_results.Operation_metadata { contents = Apply_results.Single_result _} ->
        assert false
      | Single op,
        Apply_results.Operation_metadata { contents = Apply_results.Single_result a } ->
        store_op_id db op_hash i blk_hash op a
      | Cons (op, rest),
        Apply_results.Operation_metadata { contents = Apply_results.Cons_result (a, meta)} ->
        store_op_id db op_hash i blk_hash op a >>=? fun () ->
        inner (succ i) rest (Apply_results.Operation_metadata { contents = meta })
  in
  let op_hash_str = Operation_hash.to_b58check op_hash in
  let chain_id_str = Chain_id.to_b58check chain_id in
  let blk_hash_str = Block_hash.to_b58check blk_hash in
  Sqlexpr.execute db update_operation_stmt
    op_hash_str chain_id_str blk_hash_str >>= fun () ->
  Logs_lwt.debug ~src begin fun m ->
    m "Stored raw operation %s" op_hash_str
  end >>= fun () ->
  inner 0 contents receipt

let store_blk db blk_hash
    ({ shell = { level;
                 proto_level;
                 predecessor;
                 timestamp;
                 validation_passes;
                 operations_hash;
                 fitness;
                 context }; _ } : Alpha_block_services.raw_block_header)
    ({ protocol_data =
         { baker; level = { level_position;
                            cycle; cycle_position;
                            voting_period;
                            voting_period_position; _ };
           voting_period_kind;
           nonce_hash = _;
           consumed_gas ;
           deactivated ;
           balance_updates };
       test_chain_status = _; _ } : Alpha_block_services.block_metadata) =
  let hash_str = Block_hash.to_b58check blk_hash in
  let pred_blk_hash_str = Block_hash.to_b58check predecessor in
  (* Store shell header *)
  Sqlexpr.execute db update_block_stmt
    hash_str
    level proto_level
    pred_blk_hash_str
    (Time.to_seconds timestamp) validation_passes
    (Operation_list_list_hash.to_b58check operations_hash)
    (let `Hex h =
       Hex.of_cstruct (Cstruct.of_bigarray (Fitness.to_bytes fitness)) in h)
    (Context_hash.to_b58check context) >>= fun () ->
  (* Discover deactivated pkhs and fill up deactivated table *)
  Lwt_list.iter_s begin fun pkh ->
    let pkh_str = Signature.Public_key_hash.to_b58check pkh in
    Sqlexpr.execute db discovered_implicit_stmt pkh_str >>= fun () ->
    Sqlexpr.execute db update_deactivated_stmt pkh_str hash_str >>= fun () ->
    Logs_lwt.debug (fun m -> m "Deactivated %s" pkh_str)
  end deactivated >>= fun () ->
  (* Store alpha header *)
  Sqlexpr.execute db update_block_alpha_stmt
    hash_str
    (Signature.Public_key_hash.to_b58check baker)
    level_position (Cycle.to_int32 cycle) cycle_position
    (Voting_period.to_int32 voting_period)
    voting_period_position
    (Obj.magic voting_period_kind : int)
    (Z.to_int64 consumed_gas)
  >>= fun () ->
  (* Update balances tables *)
  Balance_update.update_tables
    db ~blk_hash_str:hash_str balance_updates >>= fun () ->
  Logs_lwt.info ~src begin fun m ->
    m "Stored %s (level: %ld)" hash_str level
  end >>= fun () ->
  return_unit

let discover_initial_ks cctxt blockid db =
  Alpha_block_services.info cctxt
    ~chain:(fst blockid) ~block:(snd blockid) () >>=?
  fun { chain_id; hash;
        header = { shell = {
            level; proto_level; predecessor;
            timestamp; validation_passes;
            operations_hash; fitness; context }; _ }; _ } ->
  Alpha_services.Contract.list cctxt blockid >>=? fun ks ->
  iter_s begin fun k ->
    Alpha_services.Contract.info
      cctxt blockid k >>=? fun { manager; _ } ->
    let k_str = Contract.to_b58check k in
    let mgr_str = Signature.Public_key_hash.to_b58check manager in
    let blkh_str = Block_hash.to_b58check hash in
    (* add implicit *)
    Sqlexpr.execute
      db discovered_implicit_stmt mgr_str >>= fun () ->
    (* add chain *)
    Sqlexpr.execute
      db update_chain_stmt (Chain_id.to_b58check chain_id) >>= fun () ->
    (* add block header *)
    Sqlexpr.execute
      db update_block_stmt blkh_str level proto_level
      (Block_hash.to_b58check predecessor)
      (Time.to_seconds timestamp)
      validation_passes
      (Operation_list_list_hash.to_b58check operations_hash)
      (let `Hex h =
         Hex.of_cstruct (Cstruct.of_bigarray (Fitness.to_bytes fitness)) in h)
      (Context_hash.to_b58check context) >>= fun () ->
    (* add contract *)
    Sqlexpr.execute
      db discovered_contract_stmt k_str blkh_str mgr_str >>= fun () ->
    Logs_lwt.debug ~src (fun m -> m "Added contract %s" k_str) >>= fun () ->
    return_unit
  end ks >>=? fun _ ->
  return_unit

let bootstrap_generic
    ?(blocks_per_sql_tx=300l) ?from ?up_to cctxt db
    ~(f:#full -> Sqlexpr.db -> int32 -> unit tzresult Lwt.t) =
  Sqlexpr.select_one db select_max_level_stmt >>= fun level ->
  let from =
    match from, level with
    | Some lvl, _ -> lvl
    | None, None -> 2l
    | None, Some lvl -> Int32.succ lvl in
  begin match level with
    | Some _ -> return_unit
    | None ->
      discover_initial_ks cctxt (`Main, `Level 2l) db
  end >>=? fun () ->
  Logs.debug ~src begin fun m ->
    m "Start downloading chain from level %ld" from
  end ;
  let rec process_n_blks db counter lvl =
    match counter, up_to with
    | c, _ when c <= 0l -> return (`Counter_exhausted, lvl)
    | _, Some target when lvl > target ->
      Logs.debug ~src begin fun m ->
        m "Reached target level %ld, bootstrapping done." target
      end ;
      return (`Target_reached, lvl)
    | _ ->
      Logs.debug ~src (fun m -> m "Processing block level %ld" lvl) ;
      protect begin fun () ->
        f cctxt db lvl >>=? fun () ->
        return `Reached
      end ~on_error:begin function
        | [RPC_context.Not_found _] ->
          return `Reached_head
        | e -> Lwt.return (Error e)
      end >>=? function
      | `Reached ->
        process_n_blks db (Int32.pred counter) (Int32.succ lvl)
      | `Reached_head ->
        return (`Reached_head, lvl)
  in
  let rec inner lvl =
    let blocks_to_download =
      match up_to with
      | None -> blocks_per_sql_tx
      | Some up_to ->
        min (Int32.sub up_to lvl) blocks_per_sql_tx in
    if blocks_to_download <= 0l then begin
      Logs.debug ~src (fun m -> m "Bootstrapping done") ;
      return lvl
    end else
      Sqlexpr.transaction db begin fun db ->
        process_n_blks db blocks_to_download lvl
      end >>=? fun (status, lvl) ->
      match status with
      | `Reached_head -> return lvl
      | `Target_reached -> return lvl
      | `Counter_exhausted -> inner lvl
  in
  inner from

let store_ops db blk_hash ops =
  match ops with
  | [endorsements; _voting; anon; manager] ->
    iter_s (store_op db blk_hash) endorsements >>=? fun () ->
    iter_s (store_op db blk_hash) anon >>=? fun () ->
    iter_s (store_op db blk_hash) manager
  | _ -> assert false

let store_blk_full ?chain ?block cctxt db =
  Alpha_block_services.info cctxt ?chain ?block () >>=? fun
    { chain_id ;
      hash ;
      header ;
      metadata ;
      operations } ->
  (* Store chain *)
  Sqlexpr.execute db
    update_chain_stmt (Chain_id.to_b58check chain_id) >>= fun () ->
  (* Store block header (shell + alpha) *)
  store_blk db hash header metadata >>=? fun () ->
  (* Store ops (shell + alpha) *)
  store_ops db hash operations

let bootstrap_chain ?blocks_per_sql_tx ?from ?up_to cctxt db =
  bootstrap_generic ?blocks_per_sql_tx ?from ?up_to cctxt db
    ~f:begin fun cctxt db lvl ->
      store_blk_full ~block:(`Level lvl) cctxt db
    end

let get_delegate_info_at_snap cctxt db delegate (cycle, lvl) =
  Delegate_services.info cctxt (`Main, `Level lvl) delegate >>= function
  | Error err ->
    Logs_lwt.err begin fun m ->
      m "At %ld (cycle %a):@ %a" lvl Cycle.pp cycle pp_print_error err
    end
  | Ok ({ balance; frozen_balance;
          staking_balance;
          delegated_balance;
          deactivated; grace_period ; delegated_contracts } as d_info) ->
    Logs_lwt.info begin fun m ->
      let open Data_encoding.Json in
      m "Got delegate info for %ld (cycle %a):@ %a" lvl Cycle.pp cycle pp
        (construct Delegate_services.info_encoding d_info)
    end >>= fun () ->
    let delegate_str =
      Signature.Public_key_hash.to_b58check delegate in
    Sqlexpr.transaction db begin fun db ->
      Lwt_list.iter_s begin fun k ->
        Sqlexpr.execute db update_delegated_contract_stmt
          delegate_str
          (Contract_hash.to_b58check k)
          (Cycle.to_int32 cycle) lvl
      end delegated_contracts
    end >>= fun () ->
    Sqlexpr.execute db update_delegate_stmt
      (Cycle.to_int32 cycle)
      lvl delegate_str
      (Tez.to_mutez balance)
      (Tez.to_mutez frozen_balance)
      (Tez.to_mutez staking_balance)
      (Tez.to_mutez delegated_balance)
      deactivated
      (Cycle.to_int32 grace_period)

let bootstrap_delegate cctxt db delegate =
  snapshot_levels db >>= fun snaps ->
  let snaps = Cycle.Map.bindings snaps in
  Lwt_list.iter_s
    (get_delegate_info_at_snap cctxt db delegate) snaps >>= fun () ->
  return_unit

type delegate_info = {
  cycle: int32 ;
  level: int32 ;
  balance: int64 ;
  frozen: int64 ;
  staking: int64 ;
  delegated: int64 ;
  deactivated: bool ;
  grace_period: int32 ;
}

let fold_delegate db ~pkh f a =
  Sqlexpr.fold db begin fun a (cycle, level, balance, frozen,
                               staking, delegated, deactivated,
                               grace_period) ->
    f a begin
      { cycle ; level ; balance ; frozen ; staking ; delegated ;
        deactivated ; grace_period }
    end
  end a select_delegate_stmt
    (Signature.Public_key_hash.to_b58check pkh)

(* let load_balances cctxt blockid db =
 *   Alpha_block_services.info cctxt
 *     ~chain:(fst blockid) ~block:(snd blockid) () >>=? fun { hash ; _ } ->
 *   let blk_hash_str = Block_hash.to_b58check hash in
 *   Alpha_services.Contract.list cctxt blockid >>=? fun ks ->
 *   iter_s begin fun k ->
 *     let k_str = Contract.to_b58check k in
 *     Logs.info (fun m -> m "Added %s" k_str) ;
 *     Alpha_services.Contract.info cctxt blockid k >>=? fun { balance; _ } ->
 *     match Contract.is_implicit k with
 *     | None -> (\* originated *\)
 *       Sqlexpr.execute db update_contract_only_stmt
 *         k_str blk_hash_str >>= fun () ->
 *       Sqlexpr.execute
 *         db update_contract_balance_stmt
 *         k_str
 *         blk_hash_str
 *         None None (Tez.to_mutez balance) >>= fun () ->
 *       return_unit
 *     | Some pkh -> (\* implicit *\)
 *       let pkh_str = Signature.Public_key_hash.to_b58check pkh in
 *       Sqlexpr.execute
 *         db activated_implicit_stmt
 *         pkh_str blk_hash_str blk_hash_str >>= fun () ->
 *       Sqlexpr.execute
 *         db insert_or_replace_implicit_balance_stmt
 *         pkh_str
 *         blk_hash_str
 *         Balance_update.(int_of_category Activation)
 *         None None (Tez.to_mutez balance) >>= fun () ->
 *       return_unit
 *   end ks >>=? fun _ ->
 *   return_unit *)

let fold_contracts db f a =
  Sqlexpr.fold db begin fun a (k, mgr, _, _, _, _, _, _) ->
    match Contract.of_b58check k,
          Signature.Public_key_hash.of_b58check mgr with
    | Ok k, Ok mgr -> f a k mgr
    | _ -> Lwt.fail_with "fold_contracts"
  end a select_contract_stmt

let fold_levels db f a =
  Sqlexpr.fold db begin fun a (level, hash) ->
    f a level hash
  end a select_level_stmt

let history_one ?(display=false) chain_db k =
  find_txs_involving_k chain_db k >>= fun txs ->
  Logs.app ~src begin fun m ->
    m "Found %d transactions involving %a"
      (Operation_hash.Map.cardinal txs)
      Alpha_context.Contract.pp k
  end ;
  if display then begin
    Operation_hash.Map.iter begin fun _op_hash txs ->
      IntMap.iter begin fun _op_id tx ->
        let tx_json = Data_encoding.Json.construct tx_encoding tx in
        Logs.app ~src (fun m -> m "%a" Data_encoding.Json.pp tx_json)
      end txs
    end txs
  end ;
  return txs

let history ?display chain_db =
  map_s (history_one ?display chain_db)

let ks_of_mgr_one db mgr =
  Sqlexpr.fold db begin fun a k ->
    match Contract.of_b58check k with
    | Error _ -> Lwt.fail_with "ks_of_mgr"
    | Ok k -> Lwt.return (k :: a)
  end [] select_contract_with_mgr_stmt
    (Signature.Public_key_hash.to_b58check mgr)

let ks_of_mgr db = Lwt_list.map_s (ks_of_mgr_one db)

type balance_full = {
  level: int32 ;
  cycle: Cycle.t ;
  cycle_position: int32 ;
  op: (Operation_hash.t * int) option ;
  cat : Delegate.balance ;
  diff : int64 ;
}

let balance_full_of_sql
    (level, cycle, cycle_position, op, op_id, k_str, cat, diff) =
  let op =
    begin match op, op_id with
      | Some op, Some op_id ->
        Some (Operation_hash.of_b58check_exn op, op_id)
      | _ -> None
    end in
  let k =
    begin match Contract.of_b58check k_str with
      | Ok k -> k
      | _ -> assert false
    end in
  let cycle = (Obj.magic cycle : Cycle.t) in
  let cat =
    begin match cat with
      | 0 -> Delegate.Contract k
      | 1 -> Rewards ((Signature.Public_key_hash.of_b58check_exn k_str), cycle)
      | 2 -> Fees ((Signature.Public_key_hash.of_b58check_exn k_str), cycle)
      | 3 -> Deposits ((Signature.Public_key_hash.of_b58check_exn k_str), cycle)
      | _ -> assert false
    end in
  { level ; cycle ; cycle_position ; op ; cat ; diff }

let fold_balance_full db ?(start_level=0l) ~k f a =
  Sqlexpr.fold db begin fun a v ->
    let balance_full = balance_full_of_sql v in
    f a balance_full
  end a select_balance_full_stmt (Contract.to_b58check k) start_level

type contract_info = {
  blk: Block_hash.t ;
  mgr: public_key_hash ;
  delegate: public_key_hash option ;
  spendable: bool ;
  delegatable: bool ;
  script : Script.t option ;
}

let contract_info_encoding =
  let open Data_encoding in
  conv
    (fun { blk ; mgr ; delegate ; spendable ; delegatable ; script } ->
       (blk, mgr, delegate, spendable, delegatable, script))
    (fun (blk, mgr, delegate, spendable, delegatable, script) ->
       { blk ; mgr ; delegate ; spendable ; delegatable ; script })
    (obj6
       (req "blk" Block_hash.encoding)
       (req "mgr" Signature.Public_key_hash.encoding)
       (opt "delegate" Signature.Public_key_hash.encoding)
       (req "spendable" bool)
       (req "delegatable" bool)
       (opt "script" Script.encoding))

let get_contract_info db k =
  Sqlexpr.select_one_maybe db
    select_contract_infos_stmt (Contract.to_b58check k) >>= function
  | None -> Lwt.return_none
  | Some (blk, mgr, delegate, spendable, delegatable,_,_, script) ->
    let blk = Block_hash.of_b58check_exn blk in
    let mgr = Signature.Public_key_hash.of_b58check_exn mgr in
    let delegate =
      Option.map ~f:Signature.Public_key_hash.of_b58check_exn delegate in
    let script =
      Option.map script ~f:begin fun script ->
        match Data_encoding.Json.from_string script with
        | Error msg -> invalid_arg msg
        | Ok script_json ->
          Data_encoding.Json.destruct Script.encoding script_json
      end in
    Lwt.return_some { blk ; mgr ; delegate ; spendable ; delegatable ; script }

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
