let src = Logs.Src.create "mezos.tezos_cfg"

let host_port_of_url url =
  let tls = match Uri.scheme url with
    | None -> false
    | Some "https" -> true
    | Some _ -> false in
  match Uri.host url, Uri.port url with
  | Some host, Some port -> host, port, tls
  | _ -> invalid_arg "host_port_of_url"

let base_dir = Filename.concat (Sys.getenv "HOME") ".tezos-client"
let cfg_file base_dir = Filename.concat base_dir "config"

let mk_rpc_cfg tezos_client_dir url =
  let cfg = cfg_file tezos_client_dir in
  Client_config.read_config_file cfg >>= fun c ->
  match c, Option.map ~f:host_port_of_url url with
  | Error e, None ->
    Logs.err ~src begin fun m ->
      m "Error opening %s: %a" cfg pp_print_error e
    end ;
    invalid_arg "read_cfg_file: no file nor cmdline argument provided"
  | Error _, Some (host, port, tls) ->
    return ({ RPC_client.default_config with host ; port ; tls },
            None)
  | Ok _, Some (host, port, tls) ->
    return ({ RPC_client.default_config with host ; port ; tls },
            None)
  | Ok { node_addr; node_port; tls; confirmations }, None ->
    return ({ RPC_client.default_config with host = node_addr ;
                                             port = node_port ; tls },
            confirmations)

let mezos_full
    ?(block=`Head 0)
    ?confirmations
    ?password_filename
    ?(base_dir=base_dir)
    ?(rpc_config=RPC_client.default_config)
    () : #Client_context.full = object
  inherit Client_context_unix.unix_full
      ~base_dir ~block ~confirmations ~password_filename ~rpc_config
  method answer :
    type a. (a, unit) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Logs.app ~src (fun m -> m "%s" msg) ;
        Lwt.return_unit)
  method error :
    type a b. (a, b) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Logs.err (fun m -> m "%s" msg) ;
        Lwt.fail_with msg)
  method log : (* never used in practice *)
    type a. string -> (a, unit) Client_context.lwt_format -> a =
    fun _output -> Format.kasprintf (fun msg ->
        Logs.info (fun m -> m "%s" msg) ;
        Lwt.return_unit)
  method message :
    type a. (a, unit) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Logs.app ~src (fun m -> m "%s" msg) ;
        Lwt.return_unit)
  method warning :
    type a. (a, unit) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Logs.warn (fun m -> m "%s" msg) ;
        Lwt.return_unit)
end
