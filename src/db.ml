(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Lwt.Infix
module Sqlexpr = Sqlexpr_sqlite_lwt

let src = Logs.Src.create "mezos.db"

module type DB = sig
  val db_name : string
  val auto_init_db : Sqlite3.db -> Format.formatter -> bool
  val check_db : Sqlite3.db -> Format.formatter -> bool
  val auto_check_db : Format.formatter -> bool
end

let open_and_init_db ~datadir db =
  let module DB = (val db : DB) in
  let open DB in
  let db_path = Filename.concat datadir db_name in
  Sqlexpr.open_db db_path ~init:begin fun db ->
    let inited = auto_init_db db Format.err_formatter in
    let checked = check_db db Format.err_formatter in
    if inited && checked then begin
      Logs.debug ~src (fun m -> m "Successfully initialized DB %s" db_path) ;
    end
    else begin
      Logs.err ~src (fun m -> m "Internal error when initializing \
                            %s. Exiting." db_path) ;
      raise Exit
    end
  end

let open_db ?(pragmas=[]) ?init ~db_path () =
  let db = Sqlexpr.open_db ?init db_path in
  Lwt_list.iter_s begin fun p ->
    Sqlexpr.execute db p
  end pragmas >>= fun () ->
  Lwt.return db

let enable_foreign_keys_pragma =
  [%sqlc "PRAGMA foreign_keys = ON"]

let enable_wal_pragma =
  [%sqlc "PRAGMA journal_mode = WAL"]

let optimize_pragma =
  [%sqlc "PRAGMA optimize"]

let open_or_init_db
    ?(pragmas = [enable_foreign_keys_pragma ;
                 enable_wal_pragma])
    ~datadir db =
  let module DB = (val db : DB) in
  let open DB in
  let db_path = Filename.concat datadir db_name in
  Lwt.catch
    (fun () -> Lwt_unix.mkdir datadir 0o755)
    (fun _exn -> Lwt.return_unit) >>= fun () ->
  Lwt_unix.file_exists db_path >>= fun db_exists ->
  match db_exists with
  | true -> begin
      match auto_check_db Format.err_formatter with
      | false ->
        Logs.err ~src (fun m -> m "%s check failed. DB is outdated or \
                              corrupted. Exiting." db_path) ;
        Lwt.fail_with ("open_or_init_db: DB " ^ db_path ^ " exists but is corrupted")
      | true ->
        Logs.debug ~src (fun m -> m "Opening DB %s" db_path) ;
        open_db ~pragmas ~db_path () >>= fun db ->
        Lwt.return (db, false)
    end
  | false ->
    Lwt.catch begin fun () ->
      let db = open_and_init_db ~datadir db in
      Lwt_list.iter_s (Sqlexpr.execute db) pragmas >>= fun () ->
      Lwt.return (db, true)
    end begin fun exn ->
      Lwt.fail_with begin Printf.sprintf "Impossible to initialize %s: %s"
          db_path (Printexc.to_string exn)
      end
    end

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
