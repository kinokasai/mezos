(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Monocypher
open Tezos_crypto

module Crypto : Bip32_ed25519.CRYPTO
(** A Crypto module suitable for use with bip32-ed25519 library. *)

val zero_nonce : Bigstring.t
(** [zero_nonce] is 32 bytes of zeroes. *)

val get_wallet_extended_sk :
  Sqlexpr_sqlite_lwt.db -> string -> extended Sign.key Bip32_ed25519.t Lwt.t
(** [get_wallet_extended_sk db alias] is the extended sk corresponding
    to [alias] in [db]. A passphrase will be prompted. *)

val ed25519_pkh_of_extended :
  ?path:int32 list ->
  'a Sign.key Bip32_ed25519.t ->
  Ed25519.Public_key.t * Ed25519.Public_key_hash.t
(** [ed25519_pkh_of_extended_sk ?path k] is the Ed25519 derivation of
    [k], optionally using [path]. *)

val pkh_of_extended :
  ?path:int32 list ->
  'a Sign.key Bip32_ed25519.t ->
  Signature.public_key * Signature.public_key_hash
(** [pkh_of_extended ?path k] is the derivation of [k], optionally
    using [path].*)

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
