val forge_transfer :
  ([ `POST ], unit, unit, unit,
   (Mezos_transfer.destination Mezos_transfer.manager_operations),
   Mezos_transfer.forged_mgr_op) RPC_service.service

val originate_account :
  ([ `POST ], unit, unit, unit,
   (Mezos_transfer.origination Mezos_transfer.manager_operations),
   Mezos_transfer.forged_mgr_op) RPC_service.service

val change_delegate :
  ([ `POST ], unit, unit, unit,
   (Signature.Public_key_hash.t Mezos_transfer.manager_operations),
  Mezos_transfer.forged_mgr_op) RPC_service.service
