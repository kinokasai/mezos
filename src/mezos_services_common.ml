(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Rresult
open Alpha_environment
open Alpha_context

let derive_key =
  let node_arg =
    let open Bip32_ed25519.Human_readable in
    RPC_arg.make
      ~descr: "BIP32 node"
      ~name: "bip32_node"
      ~destruct:(fun s -> R.of_option ~none:(fun () ->
          R.error "Invalid node") (node_of_string s))
      ~construct:string_of_node () in
  RPC_service.get_service
    ~description: "Provide a BIP32-Ed25519 derivation of the master \
                   key."
    ~query:RPC_query.empty
    ~output:Tezos_crypto.Ed25519.Public_key_hash.encoding
    RPC_path.(root / "derive" /:* node_arg)

let k_arg =
  RPC_arg.make
    ~descr: "Contract"
    ~name: "contract"
    ~destruct:(fun s ->
        R.reword_error
          (fun e -> Format.asprintf "%a" pp_print_error e)
          (wrap_error (Contract.of_b58check s)))
    ~construct:Contract.to_b58check ()

let pkh_arg =
  RPC_arg.make
    ~descr: "Public key hash"
    ~name: "pkh"
    ~destruct:(fun s ->
        R.reword_error
          (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
          (R.trap_exn Signature.Public_key_hash.of_b58check_exn s))
    ~construct:Signature.Public_key_hash.to_b58check ()

let ks_query =
  let open RPC_query in
  query (fun a -> a)
  |+ multi_field
    ~descr:"Contracts to get history for" "ks" k_arg (fun a -> a)
  |> seal

let history =
  RPC_service.get_service
    ~description: "Get the history of contracts"
    ~query:ks_query
    ~output:Data_encoding.(list (list Chain_db.tx_encoding))
    RPC_path.(root / "history")

let contracts =
  let query =
    let open RPC_query in
    query (fun a -> a)
    |+ multi_field ~descr:"pkhs" "mgrs" pkh_arg (fun a -> a)
    |> seal in
  RPC_service.get_service
    ~description: "Get contracts managed by mgr"
    ~query
    ~output:Data_encoding.(list (list Contract.encoding))
    RPC_path.(root / "contracts")

let contract_info =
  RPC_service.get_service
    ~description: "Get delegate status of contracts"
    ~query:ks_query
    ~output:Data_encoding.(list (option Chain_db.contract_info_encoding))
    RPC_path.(root / "contract_info")

let missing_blocks =
  RPC_service.get_service
    ~description: "Find missing blocks in DB"
    ~query:RPC_query.empty
    ~output:Data_encoding.(list int32)
    RPC_path.(root / "missing_blocks")

let stake =
  RPC_service.get_service
    ~description:"Query Stakes DB"
    ~query:RPC_query.empty
    ~output:Data_encoding.(list Mezos_stake.fulldiff_encoding)
    RPC_path.(root / "stake" /: pkh_arg /: k_arg )

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
