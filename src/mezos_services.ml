let forge_transfer =
  RPC_service.post_service
    ~description: "Forge a transfer operation"
    ~query: RPC_query.empty
    ~input: Mezos_transfer.(manager_operations_encoding destination_encoding)
    ~output: Mezos_transfer.forged_mgr_op_encoding
    RPC_path.(root / "forge_transfer")

let originate_account =
  RPC_service.post_service
    ~description:"Originate an account"
    ~query: RPC_query.empty
    ~input: Mezos_transfer.(manager_operations_encoding origination_encoding)
    ~output: Mezos_transfer.forged_mgr_op_encoding
    RPC_path.(root / "originate_account")

let change_delegate =
  RPC_service.post_service
    ~description:"Change or remove the delegate of an account"
    ~query: RPC_query.empty
    ~input: (Mezos_transfer.manager_operations_encoding
               Signature.Public_key_hash.encoding)
    ~output: Mezos_transfer.forged_mgr_op_encoding
    RPC_path.(root / "change_delegate")
