module IntMap = Map.Make(struct
    type t = int
      let compare = Pervasives.compare end)

module FloatMap = Map.Make(struct
    type t = float
    let compare = Pervasives.compare end)

module Cache = struct
  type 'a t = {
    max_size : int ;
    mutable cache : 'a FloatMap.t ;
    mutable idx : int ;
  }

  let create ?(max_size=10) () =
    { max_size ;
      cache = FloatMap.empty ;
      idx = 0 }

  let find_opt { cache ; _ } k =
    let cur_ts = Unix.gettimeofday () in
    let v = ref None in
    try
      FloatMap.fold begin fun ts (cur_k, history) () ->
        if cur_k = k && cur_ts -. ts < 600. then begin
          v := Some history ;
          raise Exit
        end
      end cache () ;
      None
    with Exit -> !v

  let add t k history =
    if t.idx < t.max_size then begin
      t.cache <- FloatMap.add (Unix.gettimeofday ()) (k, history) t.cache ;
      t.idx <- t.idx + 1
    end
    else begin
      let new_cache =
        FloatMap.remove
          (fst (FloatMap.min_binding t.cache)) t.cache in
      t.cache <-
        FloatMap.add (Unix.gettimeofday ()) (k, history) new_cache ;
      t.idx <- t.idx + 1
    end
end
