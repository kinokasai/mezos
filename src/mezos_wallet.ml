(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Cmdliner
module Sqlexpr = Sqlexpr_sqlite_lwt
open Lwt.Infix

let src = Logs.Src.create "mezos.wallet"

let init_db datadir () =
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (_wallet_db, wallet_inited) ->
  Logs.app ~src (fun m -> m "Wallet DB inited: %b " wallet_inited) ;
  Lwt.return_unit

let init_db_cmd =
  let open Cmdliner_helpers.Terms in
  let doc = "Initialize Mezos Wallet DB." in
  Term.(const init_db $ datadir $ setup_log),
  Term.info ~doc "init"

let wallet_del datadir aliases () =
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (db, _inited) ->
  Lwt_list.iter_s begin fun a ->
    Sqlexpr.execute db Wallet_db.delete_seed_stmt a >|= fun () ->
    Logs.debug ~src (fun m -> m "Deleted wallet %s." a)
  end aliases >>= fun () ->
  Lwt.return_unit

let wallet_del_cmd =
  let open Cmdliner_helpers.Terms in
  let doc = "Delete wallets." in
  let aliases =
    let doc = "Wallets to delete." in
    Arg.(value & pos_right ~-1 string [] & info [] ~docv:"WALLET" ~doc) in
  Term.(const wallet_del $ datadir $ aliases $ setup_log),
  Term.info ~doc "del"

let wallet_show datadir alias path () =
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (db, _inited) ->
  Wallet_helpers.get_wallet_extended_sk db alias >>= fun extended_sk ->
  let _pk, pkh = Wallet_helpers.ed25519_pkh_of_extended extended_sk ~path in
  Logs.app ~src (fun m -> m "%a" Ed25519.Public_key_hash.pp pkh) ;
  Lwt.return_unit

let wallet_show_cmd =
  let open Cmdliner_helpers.Convs in
  let open Cmdliner_helpers.Terms in
  let doc = "Show the addresses in a wallet." in
  let path =
    Arg.(value & pos_right 0 bip32_node [] & info [] ~docv:"PATH" ~doc) in
  Term.(const wallet_show $ datadir $ (alias_pos 0) $ path $ setup_log),
  Term.info ~doc "show"

let wallet_list datadir () =
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (db, _inited) ->
  Wallet_db.aliases db >>= fun aliases ->
  List.iter (fun a -> Logs.app ~src (fun m -> m "%s" a)) aliases ;
  Lwt.return_unit

let wallet_list_cmd =
  let open Cmdliner_helpers.Terms in
  let doc = "List wallets." in
  Term.(const wallet_list $ datadir $ setup_log),
  Term.info ~doc "list"

let getpass () =
  Printf.printf "Enter passphrase: " ;
  let passwd = Lwt_utils_unix.getpass () in
  Printf.printf "Confirm passphrase: " ;
  let passwd2 = Lwt_utils_unix.getpass () in
  match String.equal passwd passwd2 with
  | false ->
    Logs.err ~src (fun m -> m "Passwords do not match. Aborting.") ;
    exit 1
  | true ->
    let ret = Bigstring.of_string passwd in
    Monocypher.wipe_string passwd ;
    Monocypher.wipe_string passwd2 ;
    ret

let find_acceptable_seed seed =
  let rec inner pos =
    match Bip32_ed25519.of_seed (module Wallet_helpers.Crypto) ~pos seed with
    | Some _ -> Bigstring.sub seed pos 32
    | None ->
      if pos < 32 then inner (succ pos)
      else begin
        let (_:int) = Monocypher.Hash.Blake2b.blit_digest 64 seed ~msg:seed in
        inner 0
      end
  in
  inner 0

let wallet_of_entropy mne_f datadir alias force () =
  let open Monocypher in
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (db, _inited) ->
  Wallet_db.aliases db >>= fun aliases ->
  begin if List.mem alias aliases && not force then begin
    Logs.err ~src (fun m -> m "Wallet %s already exists. Use -f to overwrite." alias) ;
    Lwt.fail_with
      (Printf.sprintf "Wallet %s already exists. Use -f to overwrite." alias)
  end
    else Lwt.return_unit
  end >>= fun () ->
  let password = getpass () in
  let mne = mne_f () in
  let seed = Bip39.to_seed ~passphrase:password mne in
  let seed = find_acceptable_seed seed in
  let salt = Rand.gen 8 in
  let key = Bigstring.create 32 in
  let mac = Bigstring.create Box.macbytes in
  let (_:int) = Pwhash.argon2i
      ~password
      ~salt key in
  Box.lock ~key ~nonce:Wallet_helpers.zero_nonce ~mac seed ;
  let cipherseed = Bytes.create (32 + Box.macbytes) in
  Bigstring.blit_to_bytes mac 0 cipherseed 0 Box.macbytes ;
  Bigstring.blit_to_bytes seed 0 cipherseed Box.macbytes 32 ;
  wipe mac ;
  wipe key ;
  Sqlexpr.execute db Wallet_db.update_seed_stmt
    alias
    (Bigstring.to_string salt)
    (Bytes.unsafe_to_string cipherseed) >>= fun () ->
  Logs.app ~src begin fun m ->
    m "Your mnemonic is:@.%a@."
      Format.(pp_print_list ~pp_sep:pp_print_space pp_print_string)
      (Bip39.to_words mne)
  end ;
  Lwt.return_unit

let mne_of_entropy () =
  let entropy = Monocypher.Rand.gen 32 in
  Bip39.of_entropy entropy

let get_mnemonic n =
  let rec inner acc i =
    if i >= n then
      List.rev acc
    else begin
      Printf.printf "Enter word %d: " (succ i) ;
      let w = Lwt_utils_unix.getpass () in
      match Bip39.index_of_word w with
      | None -> inner acc i
      | Some idx -> inner (idx :: acc) (succ i)
    end
  in
  match Bip39.of_indices (inner [] 0) with
  | None -> Pervasives.failwith "get_mnemonic"
  | Some mne -> mne

let mne_of_user_input () =
  get_mnemonic 24

let wallet_gen_cmd =
  let open Cmdliner_helpers.Terms in
  let doc = "Generate a new wallet." in
  let force =
    force ~doc:"Overwrite if exists." in
  Term.(const wallet_of_entropy $ const mne_of_entropy
        $ datadir $ (alias_pos 0) $ force $ setup_log),
  Term.info ~doc "gen"

let wallet_add_cmd =
  let open Cmdliner_helpers.Terms in
  let doc = "Input a new wallet from a mnemonic." in
  let force =
    force ~doc:"Overwrite if exists." in
  Term.(const wallet_of_entropy $ const mne_of_user_input
        $ datadir $ (alias_pos 0) $ force $ setup_log),
  Term.info ~doc "add"

let lwt_run v =
  Lwt.async_exception_hook := begin fun exn ->
    Logs.err ~src (fun m -> m "%s" (Printexc.to_string exn)) ;
  end ;
  Lwt_main.run v

let cmds =
  List.map begin fun (term, info) ->
    Term.((const lwt_run) $ term), info
  end [
    init_db_cmd ;
    wallet_gen_cmd ;
    wallet_add_cmd ;
    wallet_list_cmd ;
    wallet_del_cmd ;
    wallet_show_cmd ;
  ]

let default_cmd =
  let doc = "Mezos Wallet: Local wallet operations for Mezos" in
  Term.(ret (const (`Help (`Pager, None)))),
  Term.info ~doc "mezos-wallet"

let () = match Term.eval_choice default_cmd cmds with
  | `Error _ -> exit 1
  | #Term.result -> exit 0

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
