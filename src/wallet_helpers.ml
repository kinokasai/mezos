(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Lwt.Infix

module Crypto = struct
  let sha256 t =
    Cstruct.to_bigarray (Nocrypto.Hash.SHA256.digest (Cstruct.of_bigarray t))
  let hmac_sha512 ~key t =
    let key = Cstruct.of_bigarray key in
    let t = Cstruct.of_bigarray t in
    Cstruct.to_bigarray (Nocrypto.Hash.SHA512.hmac ~key t)
end

let zero_nonce =
  let nonce = Bigstring.create 32 in
  Bigstring.fill nonce '\x00' ;
  nonce

let get_wallet_extended_sk db alias =
  let open Monocypher in
  let open Wallet_db in
  Sqlexpr.select_one_maybe db wallet_find_stmt alias >>= function
  | None ->
    Lwt.fail_with (Printf.sprintf "Unknown alias %s. Exiting." alias)
  | Some (salt_str, seed_str) ->
    let salt = Bigstring.of_string salt_str in
    let mac = Bigstring.create Box.macbytes in
    let seed = Bigstring.create 32 in
    Bigstring.blit_of_string seed_str 0 mac 0 Box.macbytes ;
    Bigstring.blit_of_string seed_str Box.macbytes seed 0 32 ;
    Printf.printf "Enter passphrase: " ;
    let passphrase = Lwt_utils_unix.getpass () in
    let password = Bigstring.of_string passphrase in
    wipe_string passphrase ;
    let key = Bigstring.create 32 in
    let (_:int) = Pwhash.argon2i ~password ~salt key in
    match Box.(unlock ~key ~nonce:zero_nonce ~mac seed) with
    | false -> Lwt.fail_with "Invalid passphrase. Exiting"
    | true ->
      wipe key ;
      let extended_sk = Bip32_ed25519.of_seed_exn (module Crypto) seed in
      wipe seed ;
      Lwt.return extended_sk

let ed25519_pkh_of_extended ?(path=[]) extended_sk =
  let derived_extended_sk =
    Bip32_ed25519.derive_path_exn (module Crypto) extended_sk path in
  let pk =
    Monocypher.Sign.(buffer (neuterize (Bip32_ed25519.key derived_extended_sk))) in
  Bip32_ed25519.wipe derived_extended_sk ;
  let pk =
    Data_encoding.Binary.of_bytes_exn
      Ed25519.Public_key.encoding pk in
  pk, Ed25519.Public_key.hash pk

let pkh_of_extended ?(path=[]) extended_sk =
  let sk = Bip32_ed25519.derive_path_exn (module Crypto) extended_sk path in
  let pk = Bip32_ed25519.neuterize sk in
  Bip32_ed25519.wipe sk ;
  let buf = Bigstring.create 33 in
  Bigstring.set buf 0 '\x00' ;
  let _nb_written = Monocypher.Sign.blit (Bip32_ed25519.key pk) buf 1 in
  let pk =
    Data_encoding.Binary.of_bytes_exn
      Signature.Public_key.encoding buf in
  pk, Signature.Public_key.hash pk

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
