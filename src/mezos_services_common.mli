(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Alpha_environment
open Alpha_context

val derive_key :
  ([ `GET ],
   unit,
   unit * Bip32_ed25519.Human_readable.node list,
   unit,
   unit, Ed25519.Public_key_hash.t) RPC_service.service

val history :
  ([ `GET ],
   unit,
   unit,
   Contract.contract list,
   unit,
   Chain_db.tx list list) RPC_service.service

val contracts :
  ([ `GET ],
   unit,
   unit,
   public_key_hash list,
   unit,
   Contract.t list list) RPC_service.service

val contract_info :
  ([ `GET ],
   unit,
   unit,
   Contract.t list,
   unit,
   Chain_db.contract_info option list) RPC_service.service

val missing_blocks :
  ([ `GET ],
   unit,
   unit,
   unit,
   unit,
   int32 list) RPC_service.service

val stake :
  ([ `GET ],
   unit,
   (unit * public_key_hash) * Contract.t,
   unit,
   unit,
   Mezos_stake.fulldiff list) RPC_service.service

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
