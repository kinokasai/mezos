module Sqlexpr = Sqlexpr_sqlite_lwt
open Cmdliner

let src = Logs.Src.create "mezos.bootstrap"

(* let load_balances tezos_url tezos_client_dir datadir level () =
 *   Sqlexpr.set_retry_on_busy true ;
 *   Db.open_or_init_db
 *     ~datadir (module Chain_db) >>= fun (chain_db, _inited) ->
 *   Tezos_cfg.mk_rpc_cfg
 *     tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
 *   let cctxt =
 *     new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
 *   Chain_db.load_balances cctxt (`Main, `Level level) chain_db >>=? fun () ->
 *   return_unit *)

let snapshot_levels datadir () =
  Sqlexpr.set_retry_on_busy true ;
  Db.open_or_init_db
    ~datadir (module Chain_db) >>= fun (db, _inited) ->
  let open Tzscan in
  V3.snapshot_levels () >>= function
  | `Ok (Some lvls) ->
    let lvls = List.sort Int32.compare lvls in
    let lvls = List.mapi (fun i l -> i+7, l) lvls in
    Chain_db.store_snapshot_levels db lvls >>= fun () ->
    return_unit
  | #RPC.service_result ->
    failwith "error while querying TZScan"

let bootstrap_delegate
    discover tezos_url
    tezos_client_dir datadir delegate () =
  Sqlexpr.set_retry_on_busy true ;
  Db.open_or_init_db
    ~datadir (module Chain_db) >>= fun (db, _inited) ->
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
  let cctxt =
    new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  begin if discover then
      Chain_db.discover_initial_ks cctxt (`Main, `Level 2l) db
    else return_unit
  end >>=? fun () ->
  Chain_db.bootstrap_delegate cctxt db delegate

let bootstrap
    discover from up_to blocks_per_sql_tx
    tezos_url tezos_client_dir datadir () =
  Sqlexpr.set_retry_on_busy true ;
  Db.open_or_init_db
    ~datadir (module Chain_db) >>= fun (db, _inited) ->
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
  let cctxt =
    new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  begin if discover then
      Chain_db.discover_initial_ks cctxt (`Main, `Level 2l) db
    else return_unit
  end >>=? fun () ->
  Chain_db.bootstrap_chain
    cctxt ?blocks_per_sql_tx ?from ?up_to db >>=? fun reached ->
  Logs_lwt.app ~src begin fun m ->
    m "Reached level %ld, exiting." reached
  end >>= fun () ->
  return_unit

(* let load_balances_cmd =
 *   let open Cmdliner_helpers.Terms in
 *   let from = Arg.(value & opt int32 2l &
 *                   info ["from"] ~doc:"Which block level") in
 *   let doc = "Load first Alpha block and store it in SQLite." in
 *   Term.(const load_balances $
 *         uri ~args:["tezos-url"] ~doc:"URL of a running Tezos node" $
 *         tezos_client_dir $
 *         datadir $ from $ setup_log),
 *   Term.info ~doc "load-balances" *)

let discover =
  let doc = "Discover addrs in first block" in
  Arg.(value & flag & info ~doc ["first-block"])

let bootstrap_cmd =
  let open Cmdliner_helpers.Terms in
  let doc = "Download Tezos and store it in SQLite." in
  let from = Arg.(value & opt (some int32) None &
                  info ["from"] ~doc:"Where to start") in
  let up_to = Arg.(value & opt (some int32) None &
                   info ["upto"] ~doc:"Where to stop") in
  let blocks_per_sql_tx = Arg.(value & opt (some int32) None &
                               info ["save-increment"]
                                 ~doc:"How often to synchronize on disk") in
  Term.(const bootstrap $ discover $ from $ up_to $ blocks_per_sql_tx $
        uri ~args:["tezos-url"] ~doc:"URL of a running Tezos node" $
        tezos_client_dir $
        datadir $ setup_log),
  Term.info ~doc "all"

let bootstrap_delegate_cmd =
  let open Cmdliner_helpers.Terms in
  let open Old_alpha_cmdliner_helpers.Convs in
  let doc = "Download delegate information in SQLite." in
  let delegate =
    Arg.(required & pos 0 (some address) None & info [] ~docv:"PKH") in
  Term.(const bootstrap_delegate $ discover $
        uri ~args:["tezos-url"] ~doc:"URL of a running Tezos node" $
        tezos_client_dir $ datadir $ delegate $ setup_log),
  Term.info ~doc "delegate"

let snapshot_levels_cmd =
  let open Cmdliner_helpers.Terms in
  let open Old_alpha_cmdliner_helpers.Convs in
  let doc = "Get snapshot levels from TZScan and store in SQLite." in
  Term.(const snapshot_levels $ datadir $ setup_log),
  Term.info ~doc "snapshot-levels"

let build_stake_db datadir delegate related start_cycle end_cycle () =
  Sqlexpr.set_retry_on_busy true ;
  Db.open_or_init_db
    ~datadir (module Chain_db) >>= fun (db, _inited) ->
  Mezos_stake.build_stake_db db
    ~related ?start_cycle ?end_cycle delegate >>= fun _ ->
  return_unit

let build_stake_db_cmd =
  let open Cmdliner_helpers.Terms in
  let open Old_alpha_cmdliner_helpers.Convs in
  let doc = "Build the stake DB." in
  let delegate =
    Arg.(required & pos 0 (some address) None & info [] ~docv:"PKH") in
  let related =
    Arg.(value & pos_right 0 contract [] & info [] ~docv:"KS") in
  let start_cycle =
    Arg.(value & opt (some int32) None &
         info ["s" ; "start-cycle"] ~doc:"First cycle to process") in
  let end_cycle =
    Arg.(value & opt (some int32) None &
         info ["e" ; "end-cycle"] ~doc:"Max cycle to process") in
  Term.(const build_stake_db $ datadir $ delegate $
        related $ start_cycle $ end_cycle $ setup_log),
  Term.info ~doc "build-stake-db"

let lwt_run v =
  Lwt.async_exception_hook := begin fun exn ->
    Logs.err ~src (fun m -> m "%a" pp_exn exn) ;
  end ;
  match Lwt_main.run v with
  | Error err ->
    Logs.err ~src (fun m -> m "%a" pp_print_error err) ;
    exit 1
  | Ok () -> ()

let cmds =
  List.map begin fun (term, info) ->
    Term.((const lwt_run) $ term), info
  end [
    (* load_balances_cmd ; *)
    build_stake_db_cmd ;
    snapshot_levels_cmd ;
    bootstrap_cmd ;
    bootstrap_delegate_cmd ;
  ]

let default_cmd =
  let doc = "Download Tezos and store it in SQLite." in
  Term.(ret (const (`Help (`Pager, None)))),
  Term.info ~doc "mezos-bootstrap"

let () = match Term.eval_choice default_cmd cmds with
  | `Error _ -> exit 1
  | #Term.result -> exit 0
