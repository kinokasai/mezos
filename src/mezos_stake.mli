open Alpha_context

module KMap : Map.S with type key = Contract.t
module Int32Map : Map.S with type key = int32

type diff = {
  amount : int64 ;
  kind : [`Contract | `Rewards] ;
}

val diff_encoding : diff Data_encoding.t

type fulldiff = {
  level : int32 ;
  diff : diff ;
}

val create_fulldiff :
  level:int32 ->
  kind:[`Contract | `Rewards] ->
  amount:int64 -> fulldiff

val fulldiff_encoding : fulldiff Data_encoding.t

type t = (int64 * (diff list)) Int32Map.t KMap.t

val empty : t

val recover_stake_db :
  ?max_level:int32 ->
  Sqlexpr_sqlite_lwt.db ->
  public_key_hash -> t Lwt.t

val build_stake_db :
  Sqlexpr_sqlite_lwt.db ->
  ?related:Contract.t list ->
  ?start_cycle:int32 ->
  ?end_cycle:int32 ->
  public_key_hash -> t Lwt.t

