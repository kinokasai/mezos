(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

include Mezos_db_common.Db.DB

module Sqlexpr = Sqlexpr_sqlite_lwt

module AC = Alpha_context
open AC

module Balance_update : sig
  type t = (Delegate.balance * Delegate.balance_update) list

  val sum_tezs : Tez.tez list -> Tez.t tzresult Lwt.t
  val avg_tezs : Tez.tez list -> Tez.t tzresult Lwt.t
end

val store_blk_full :
  ?chain:Block_services.chain ->
  ?block:Block_services.block ->
  #full -> Sqlexpr.db -> unit tzresult Lwt.t

type tx = {
  op_hash : Operation_hash.t ;
  id : int ;
  blk_hash : Block_hash.t ;
  level : int ;
  timestamp : Time.t ;
  src : Contract.t ;
  src_mgr : Signature.public_key_hash option ;
  dst : Contract.t ;
  dst_mgr : Signature.public_key_hash option ;
  amount : Tez.t ;
  fee : Tez.t ;
}

val tx_encoding : tx Data_encoding.t

val find_txs_involving_k :
  Sqlexpr.db -> Contract.t -> tx IntMap.t Operation_hash.Map.t Lwt.t

val fold_contracts :
  Sqlexpr.db ->
  ('a -> Contract.t -> Signature.public_key_hash -> 'a Lwt.t) ->
  'a -> 'a Lwt.t

val fold_levels :
  Sqlexpr.db ->
  ('a -> int32 -> string -> 'a Lwt.t) -> 'a -> 'a Lwt.t

val bootstrap_generic :
  ?blocks_per_sql_tx:int32 ->
  ?from:int32 ->
  ?up_to:int32 ->
  (#full as 'a) ->
  Sqlexpr.db ->
  f:('a -> Sqlexpr.db -> int32 -> unit tzresult Lwt.t) ->
  int32 tzresult Lwt.t

val bootstrap_chain :
  ?blocks_per_sql_tx:int32 ->
  ?from:int32 -> ?up_to:int32 ->
  #full ->
  Sqlexpr.db ->
  int32 tzresult Lwt.t
(** [bootstrap_chain ?blocks_per_sql_tx ?from ?up_to cctxt db]
    download and store blocks from [cctxt] in [db]. [blocks_per_sql_tx]
    governs how many blocks will be downloaded before commiting them in
    SQLite. [from] is the starting point for downloading the chain
    (default [2l]). [up_to] is a target block where to stop downloading
    blocks. *)

val bootstrap_delegate :
  #full ->
  Sqlexpr.db ->
  Signature.Public_key_hash.t ->
  unit tzresult Lwt.t
(** [bootstrap_delegate cctxt db delegate] downloads and stores
    delegate info for [delegate] from [cctxt] in [db]. *)

(* val load_balances :
 *   #full ->
 *   Block_services.chain * Block_services.block ->
 *   Sqlexpr.db ->
 *   unit tzresult Lwt.t
 * (\** [load_balances cctxt blkid db] loads the balances of all contracts
 *     present on the chain at [blkid] in [db]. *\) *)

val discover_initial_ks :
  #full ->
  Block_services.chain * Block_services.block ->
  Sqlexpr.db ->
  unit tzresult Lwt.t
(** [discover_initial_ks cctxt blkid db] discovers all contracts
    present on the chain at [blkid] and write in [db]. *)

val history :
  ?display:bool ->
  Sqlexpr.db ->
  Contract.t list ->
  tx IntMap.t Operation_hash.Map.t list tzresult Lwt.t
(** [history ?cache ?display db ks] returns the lists of transactions
    found in [db], corresponding to [k]. *)

val ks_of_mgr :
  Sqlexpr.db -> public_key_hash list -> Contract.t list list Lwt.t
(** [ks_of_mgr db pkh] returns the list of contracts managed by [pk]
    stored in [db]. *)

val store_snapshot_levels :
  Sqlexpr.db -> (int * int32) list -> unit Lwt.t

val snapshot_levels :
  Sqlexpr.db -> int32 Cycle.Map.t Lwt.t

type balance_full = {
  level: int32 ;
  cycle: Cycle.t ;
  cycle_position: int32 ;
  op: (Operation_hash.t * int) option ;
  cat : Delegate.balance ;
  diff : int64 ;
}

val fold_balance_full :
  Sqlexpr.db -> ?start_level:int32 -> k:Contract.t ->
  ('a -> balance_full -> 'a Lwt.t) -> 'a -> 'a Lwt.t

type delegate_info = {
  cycle: int32 ;
  level: int32 ;
  balance: int64 ;
  frozen: int64 ;
  staking: int64 ;
  delegated: int64 ;
  deactivated: bool ;
  grace_period: int32 ;
}

val fold_delegate :
  Sqlexpr.db -> pkh:public_key_hash ->
  ('a -> delegate_info -> 'a Lwt.t) -> 'a -> 'a Lwt.t

type contract_info = {
  blk: Block_hash.t ;
  mgr: public_key_hash ;
  delegate: public_key_hash option ;
  spendable: bool ;
  delegatable: bool ;
  script : Script.t option ;
}

val contract_info_encoding :
  contract_info Data_encoding.encoding
val get_contract_info :
  Sqlexpr.db -> Contract.t -> contract_info option Lwt.t

val update_stake :
  delegate:public_key_hash ->
  level:int32 ->
  k:Contract.t ->
  kind:[< `Contract | `Rewards ] ->
  amount:int64 ->
  Sqlexpr.db -> unit Lwt.t

val fold_stake_full :
  ?max_level:int32 -> (** inclusive *)
  Sqlexpr.db ->
  delegate:public_key_hash ->
  f:('a ->
     k:Contract.t ->
     level:int32 ->
     kind:[`Contract | `Rewards] ->
     amount:int64 -> 'a Lwt.t) ->
  init:'a -> 'a Lwt.t

val fold_stake :
  Sqlexpr.db ->
  delegate:public_key_hash ->
  k:Contract.t ->
  f:('a ->
     level:int32 ->
     kind:[`Contract | `Rewards] ->
     amount:int64 -> 'a Lwt.t) ->
  init:'a -> 'a Lwt.t

val nb_alpha_operations :
  Sqlexpr.db -> Operation_hash.t -> int Lwt.t

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
